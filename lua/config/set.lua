-- Basic Settings
vim.opt.title = true
vim.opt.go = a
vim.opt.mouse = a
vim.opt.hlsearch = false
-- vim.opt.clipboard:append { 'unnamed', 'unnamedplus' } -- Synchronize vim register with system clipboard
vim.opt.showmode = false
vim.opt.showcmd = false
vim.opt.filetype = plugin
vim.opt.syntax = 'on'
vim.opt.smartcase = true
vim.opt.compatible = false

vim.opt.ruler = false
vim.opt.laststatus = 0

-- Timeout settings
vim.opt.timeout = true
vim.opt.timeoutlen = 500

-- Numbers and Relativenumbers
vim.opt.nu = true
vim.opt.relativenumber = true

-- Tab Settings
vim.opt.tabstop = 8
vim.opt.softtabstop = 8
vim.shiftwidth = 4
vim.opt.expandtab = false
vim.opt.splitbelow = true
vim.opt.splitright = true

-- Smart Settings
vim.opt.smartcase = true
vim.opt.smartindent = true

-- Linewrap
-- vim.opt.wrap = false

-- Splits
vim.opt.splitbelow = true
vim.opt.splitright = true


-- Swapfile & Undo Settings
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("XDG_CONFIG_HOME") .. "/nvim/undodir"
vim.opt.undofile = true

-- Visual Aids
vim.opt.cursorline = true
-- vim.opt.colorcolumn = "80"

-- Tab completion
vim.opt.wildmode = longest,list,full
vim.opt.path:append {'**'}


vim.opt.incsearch = true

vim.opttermguicolors = true

vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50
