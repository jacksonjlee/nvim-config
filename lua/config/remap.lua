vim.g.mapleader = " "
vim.g.maplocalleader = ","

-- Open file browser
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Toggle orthography check
vim.keymap.set("n", "<leader>o", ":setlocal spell! spelllang=en_us<CR>")
vim.keymap.set("v", "<leader>o", ":setlocal spell! spelllang=en_us<CR>")

-- Toggle highlights
vim.keymap.set("n", "<leader>k", ":set hls!<CR>")
vim.keymap.set("v", "<leader>k", ":set hls!<CR>")

-- Toggle relative numbers
vim.keymap.set("n", "<leader>r", ":set relativenumber!<CR>")    -- Toggle relativenumbers
vim.keymap.set("v", "<leader>r", ":set relativenumber!<CR>")
vim.keymap.set("n", "<leader>R", ":set number!<CR>")    -- Toggle numbers
vim.keymap.set("v", "<leader>R", ":set number!<CR>")

-- Yanking
vim.keymap.set("n", "<leader>y", "\"+y") -- Yank to system clipboard
vim.keymap.set("v", "<leader>y", "\"+y") -- Yank selection to system clipboard
vim.keymap.set("n", "<leader>Y", "\"+Y") -- Yank cursor to EOL to system clipboard

-- -- Placeholders and shortcuts
vim.keymap.set("n", "<localleader>.", "a<++><esc>")     -- Place placeholder
vim.keymap.set("i", "<localleader>.", "<++>")
vim.keymap.set("n", "<localleader>,", "<Esc>/<++><Enter>c4l")   -- Jump to placeholder, delete it, and enter insert mode
vim.keymap.set("i", "<localleader>,", "<Esc>/<++><Enter>c4l")

-- -- Split shortcuts
vim.keymap.set("n", "Q", "<nop>")       -- Never press Q without this

-- Navigate between splits with Ctrl+hjkl
vim.keymap.set("n", "<C-h>", "<C-w>h")
vim.keymap.set("n", "<C-j>", "<C-w>j")
vim.keymap.set("n", "<C-k>", "<C-w>k")
vim.keymap.set("n", "<C-l>", "<C-w>l")

-- Buffer shortcuts
vim.keymap.set("n", "<leader>bb", ":buffers<CR>:buffer<space>") -- List buffers available to swap between
vim.keymap.set("n", "<leader>bk", ":buffers<CR>:bd!<space>")    -- List buffers available to kill

vim.keymap.set("n", "<leader>i", ":set paste!") -- Toggle Paste mode


vim.cmd([[
" autocmd Settings
" Fix .local file type set (for fail2ban)
	autocmd BufRead,BufNewFile *.local set filetype=config


"""""""" Features and Keybindings""""""""
" Toggle highlighting
	map <leader>k :set hls!<CR>
" Toggle relativenumbers
	map <leader>r :set relativenumber!<CR>
" Split shortcuts and settings
	nnoremap <silent> <leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
	nnoremap <silent> <leader>- :exe "resize " . (winheight(0) * 2/3)<CR>
" Tab shortcuts and settings
	map <C-t> :tabnew<CR>
	noremap tt :tab split<CR>
" Open Specific Files
	nnoremap <leader>=c :edit ~/.config/nvim/init.vim<CR>
	nnoremap <leader>=r :edit ~/Documents/org/refile.org<CR>
	map <leader>=b :vsp ~/Documents/bibrefs/blog.bib<CR>

" Buffer control
	map <leader>bb :buffers<CR>:buffer<space>
	map <leader>bk :buffers<CR>:bd!<space>

" Create the 'tags' file using ctags
	command! MakeTags !ctags -R .
" Find and replace
	nnoremap S :%s//g<Left><Left>
" Disable automatic commenting on newline:
	autocmd Filetype * setlocal formatoptions-=cro

" Delete all trailing whitespaces and newlines at the end of file on save and maintain cursor position
	autocmd BufWritePre * let currPos = getpos(".")
	autocmd BufWritePre * %s/\s\+$//e
	autocmd BufWritePre * %s/\n\+\%$//e
	autocmd BufWritePre *.[ch] %s/\%$/\r/e
	autocmd BufWritePre * cal cursor(currPos[1], currPos[2])

" Automatically run xrdb
	autocmd BufRead,BufNewFile Xresources,xresources,Xdefaults,xdefaults set filetype=xdefaults
	autocmd BufWritePost xresources,Xresources,Xdefaults,xdefaults !xrdb %


]])
