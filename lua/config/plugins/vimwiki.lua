return {
	"vimwiki/vimwiki",
	init = function()
		vim.g.vimwiki_list = {
			{
			path = "$HOME/Vault/Vimwiki",
			syntax = "markdown", ext = "md"
			},
		}
	end,
	-- vim.api.nvim_create({"BufNewFile"}, {
	-- pattern = {"$HOME/Vault/Vimwiki/diary[0-9]*.md"},
	-- callback = function() 
	-- 	:silent ! 0r !echo "Works"
	-- end,
}
