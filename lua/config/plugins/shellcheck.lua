return {
        "itspriddle/vim-shellcheck",
	config = function()
		local keymap = vim.keymap

                vim.cmd("map <leader>p :ShellCheck!")
	end
}
