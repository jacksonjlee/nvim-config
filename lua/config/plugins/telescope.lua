return {
	"nvim-telescope/telescope.nvim",
	dependencies = { "nvim-lua/plenary.nvim" },
	config = function()
		local keymap = vim.keymap
		local builtin = require("telescope.builtin")
		keymap.set("n", "<leader>ff", "<cmd>Telescope find_files<CR>")
	end
}
