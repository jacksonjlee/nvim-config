return {
        "folke/zen-mode.nvim",
        opts = {
                window = {
                        backdrop = 1,
                        width = function()
                                return math.min(120, vim.o.columns * 0.75)
                        end,
                        height = 0.9,
                        options = {
                                number = false,
                                relativenumber = false,
                                foldcolumn = "0",
                                list = false,
                                showbreak = "NONE",
                                signcolumn = "no",

                        },
                },
                plugins = {
                        options = {
                                cmdheight = 1,
                                laststatus = 0,
                        },
                },
        },
        keys = {
                        { "<leader>z", "<cmd>ZenMode<CR>", desc = "Toggle ZenMode" },
        }
}
